
var slideIndex = 0;

$(document).ready(function () {
    slides();
});


function slides(){
    var i;
    var x = document.getElementsByClassName("mySlides");

    for (i = 0; i < x.length; i++)
    {
        x[i].style.display = "none";
    }
    slideIndex++;
    if (slideIndex > x.length)
    {
        slideIndex = 1
    }
    x[slideIndex-1].style.display = "block";
    // the time is set to change image every 3 secs
    setTimeout(slides, 2000);
}