
function doValidate_frmAddLogin() {
    var form = $("#frmLogin");
    form.validate({
        rules:{
            txtFirstName:{
                required: true,
            },
            txtLastName:{
                required: true,
            },
            txtEmailAddress:{
                required: true,
            },
            txtPassword:{
                required: true,
            },
            txtRepeatPassword:{
                required: true,
            }
        },
        messages:{
            txtFirstName: {
                required: "Please enter first name"
            },
            txtEmailAddress:{
                required: "Email address must be entered"
            },
            txtPassword: {
                required: "password must be entered"
            },
            txtRepeatPassword: {
                required: "Please enter the correct password"
            }
        }
    });
    return form.valid();
}